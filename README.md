# Pleroma Emoji Stealer

A way to steal Pleroma Emojis using an MRF and a Python script.

# Usage

# MRF
### ***NOW IN THE PLEROMA DEV REPO***

Simply enable it in Admin-FE under the name `Pleroma.Web.ActivityPub.MRF.StealEmojiPolicy`. This can also be done in the [config file](https://docs-develop.pleroma.social/backend/configuration/cheatsheet/#message-rewrite-facility).

### Configuring the MRF

This can either be done in the [config file](https://docs-develop.pleroma.social/backend/configuration/cheatsheet/#mrf_steal_emoji) or in Admin-FE after enabling it.

*Note that emoji stealing is* __whitelisted__*, meaning you have to add all the instances you want to steal emojis from yourself.*

## Python script
The python script requires one argument: a path to the emoji folder (by default this is the `instance/static/emoji/stolen` of your Pleroma install). By default, the JSON is printed into stdout which can be redirected using the `-o` flag

### Example usage:
```
python3 grabber.py /opt/pleroma/instance/static/emoji/stolen -o /opt/pleroma/instance/static/emoji/stolen/pack.json
```
This can be added as a cronjob to refresh the stolen emoji pack.

# KNOWN LIMITATIONS:
- Some files do not get the proper extensions added.
- You still have to manually refresh the emojis to get the ones added after the script runs.
