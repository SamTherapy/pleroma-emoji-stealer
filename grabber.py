#!/usr/bin/env python3
import os
import argparse
import json


def get_images(path):
    images = {"files":{},"pack":{},"files_count":0}
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(".png") or file.endswith(".jpg") or file.endswith(".gif"):
                images["files"][file.split(".")[0]] = file
    return images

def write_json(path, data):
    with open(path, "w") as f:
        json.dump(data, f, indent=4)

def main():
        parser = argparse.ArgumentParser()
        parser.add_argument("path", nargs=1, help="path to the folder with images")
        parser.add_argument("-o", "--output", help="path to the output file")
        args = parser.parse_args()
        images = get_images(args.path[0])
        if args.output:
            write_json(args.output, images)
        else:
            print(json.dumps(images, indent=4))

if __name__ == "__main__":
    main()
